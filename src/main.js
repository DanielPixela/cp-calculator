import { createApp } from 'vue'
import App from './App.vue'

// Import bootstrap
import 'bootstrap/scss/bootstrap.scss';

// Global styles
import '@/assets/app.scss';


createApp(App).mount('#app')
